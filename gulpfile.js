
const gulp = require('gulp') 
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const minifycss = require('gulp-minify-css')
const sourcemaps = require('gulp-sourcemaps')
const browserSync = require('browser-sync')
const extend = require('extend')
const fs = require('fs')

var defaults = {
  path: '.',
  buildPath: './build',
  browserSync: {
    open: false,
    port: 3000,
    files: [
      '**/*.css',
      '**/*.html'
    ],
    server: {
      baseDir: './'
    }
  },
  sass: {
    errLogToConsole: true,
    outputStyle: 'compact',
    includePaths: [
      __dirname + '/node_modules/bootstrap-sass/assets/stylesheets/'
    ],
  },
  minifycss: {
    keepBreaks: true
  },
  autoprefixer: {
    browsers: ['> 1%', 'IE 8', 'IE 9', 'Firefox > 10', 'iOS > 6'],
    cascade: false,
  }
}

var config = {}
if(fs.existsSync('.gulp.json')) {
  config = require('.gulp.json')
}

config = extend(true, defaults, config)

gulp.task('sass', function() {
  return gulp.src(config.path + '/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(config.sass)).on('error', sass.logError)
    .pipe(autoprefixer(config.autoprefixerO))
    .pipe(minifycss(config.minifycss))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.buildPath))
})

gulp.task('serve', ['sass'], function(done) {
  browserSync(config.browserSync, done)
})

gulp.task('watch', ['serve'], function() {
  gulp.watch(config.path + '/sass/**/*.scss', ['sass'])
})

gulp.task('default', ['sass', 'watch'])
